package tn.project.easy.stock.easy.stock.api;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tn.project.easy.stock.easy.stock.business.EntrepotService;
import tn.project.easy.stock.easy.stock.data.Entrepot;
import tn.project.easy.stock.easy.stock.data.EntrepotRepository;
import tn.project.easy.stock.easy.stock.data.Produit;


@RestController
@RequestMapping(value = "/api")
public class EntrepotRestController {
	
	  @Autowired
	    private EntrepotService entreportService;
      @Autowired
      EntrepotRepository entrepotRepository;
	   
	    @PostMapping("/entrepot")
	    void create(@RequestBody Entrepot e) {
	        try {
	            this.entreportService.ajouterEntrepot(e);
	        } catch (Exception ex) {
	            System.out.println(ex);
	        }
	    }
	    @PostMapping("/entrepot/addProduit")
	    void addProduitToEntrepot(@RequestBody Produit p,Entrepot e) {
	    	try {
	       entreportService.affecterProduitToEntrepot(p, e);
	    	} catch (Exception ex) {
	    		System.out.print(ex);
	    	}
	    }
	    @PutMapping("/entrepot/{entrepot_id1}/{entrepot_id2}")
	    public void moveProducts(@PathVariable("entrepot_id1") Long entrepotId1, @PathVariable("entrepot_id2") Long entrepotId2)
	    {    
	        entreportService.deplacerProduits(entrepotId1, entrepotId2);
	    }
	    
	/*    @GetMapping("/entrepots")
	    public List<Entrepot> findAll()
	    {
	       
			return entrepotRepository.findAll();
	    }


	    @PutMapping("/entrepot/{entrepot_id}")
	    public Entrepot update(@PathVariable("entrepot_id") Long entrepotId, @RequestBody Entrepot entrepotObject)
	    {
	        Entrepot entrepot = entrepotRepository.findOne(entrepotId);
	        entrepot.setNom(entrepotObject.getNom());
	        entrepot.setAdresse(entrepotObject.getAdresse());
	        return entrepotRepository.save(entrepot);
	    }



	    @DeleteMapping("/entrepot/{entrepot_id}")
	    public List<Entrepot> delete(@PathVariable("entrepot_id") Integer entrepotId)
	    {
	    	entrepotRepository.deleteById(entrepotId);
	        return entrepotRepository.findAll();
	    }



	    @GetMapping("/entrepot/{entrepot_id}")
	    public Entrepot findByUserId(@PathVariable("entrepot_id") Integer entrepotId)
	    {
	        return entrepotRepository.findOne(entrepotId);
	    } */


}
