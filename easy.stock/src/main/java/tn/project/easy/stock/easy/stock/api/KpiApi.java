package tn.project.easy.stock.easy.stock.api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tn.project.easy.stock.easy.stock.business.ProduitServices;



public class KpiApi {
	@Autowired
	private ProduitServices produitService;

	@RequestMapping(path="/produits/moyenne-produit-par-entrepot",method = RequestMethod.GET)
	public Long getMoyProduitParEntrepot(){
		return produitService.moyProduitParEntrepot();  
	}

}