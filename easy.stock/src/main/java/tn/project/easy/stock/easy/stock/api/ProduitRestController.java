package tn.project.easy.stock.easy.stock.api;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonIgnore;

import tn.project.easy.stock.easy.stock.business.ProduitServices;
import tn.project.easy.stock.easy.stock.data.Produit;

@RestController
public class ProduitRestController {

	@Autowired
	private ProduitServices produitService;
  
    @PostMapping("/produits/add-produit")
    void addProduct(@RequestBody Produit p) {
      produitService.ajouterProduit(p);
    }
	
	@JsonIgnore
	@RequestMapping(path="/produits",method = RequestMethod.GET)
	public List<Produit> getAll(){
		return produitService.getProduits();
	}
	@RequestMapping(path="/produits/entrepot",method = RequestMethod.GET)
	public List<Integer> getNbreProduitParEntrepot(){
		return produitService.nbreProduitParEntrepot();  
	}
	@RequestMapping(path="/produits/moyenne-produit-par-entrepot",method = RequestMethod.GET)
	public Long getNbreMoyProduitParEntrepot(){
		return produitService.moyProduitParEntrepot();  
	}
	
	@RequestMapping(path="/produits/moyenne-quantite",method = RequestMethod.GET)
	public Integer getMoyQuantiteParProduit(){
		return produitService.avgQuantite();   
	}
	@RequestMapping(path="/produits/pourcentage",method = RequestMethod.GET)
	public Double getPourcentageProduit(){
		return produitService.pourcentagePdtAffectés();   
	}
	 @DeleteMapping("/produits/delete/{produit_id}")
	    @ResponseBody
	    public void delete(@PathVariable("produit_id") Long produitId)
	    {	try {
	   produitService.supprimerProduit(produitId);
	    }catch(Exception ex) {
	    	System.out.print(ex);
	    }
	    }

}
