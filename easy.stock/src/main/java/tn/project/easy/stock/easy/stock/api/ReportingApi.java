package tn.project.easy.stock.easy.stock.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tn.project.easy.stock.easy.stock.business.EntrepotService;



@RestController
public class ReportingApi {

	  @Autowired
	    private EntrepotService entreportService;
	  @RequestMapping(path="/entrepots/ville/{ville}",method = RequestMethod.GET)
		public Long getNbreEntrepotParVille(@PathVariable("ville") String ville){
			return entreportService.nbreEntrepotParVille(ville); 
		}

	  @RequestMapping(path="/entrepots/pays/{pays}",method = RequestMethod.GET)
		public Long getNbreEntrepotParPays(@PathVariable("pays") String pays){
			return entreportService.nbreEntrepotParPays(pays); 
		}
}