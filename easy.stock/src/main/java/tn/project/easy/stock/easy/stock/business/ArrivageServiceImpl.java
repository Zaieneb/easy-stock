package tn.project.easy.stock.easy.stock.business;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.project.easy.stock.easy.stock.data.Arrivage;
import tn.project.easy.stock.easy.stock.data.ArrivageRepository;
import tn.project.easy.stock.easy.stock.data.Entrepot;
import tn.project.easy.stock.easy.stock.data.Produit;


@Service
public class ArrivageServiceImpl implements ArrivageServices {

	@Autowired
	private ArrivageRepository ar;

	@Override
	public List<Arrivage> getArrivages() {
		return ar.findAll();
	}

	@Override
	public List<Produit> getArrivageProduits(Long arrId) {
		return ar.findById(arrId).get().getProduits();
	}

	@Override
	public List<Entrepot> getArrivageEntrepots(Long arrId) {
		List<Entrepot> entrepots = new ArrayList<Entrepot>();
		getArrivageProduits(arrId).stream().forEach(p ->
			p.getEntrepots().stream().forEach(e-> entrepots.add(e))
		);
		return entrepots.stream().distinct().collect(Collectors.toList());
	}

	
}

