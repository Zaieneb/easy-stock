package tn.project.easy.stock.easy.stock.business;
import java.util.List;

import tn.project.easy.stock.easy.stock.data.Arrivage;
import tn.project.easy.stock.easy.stock.data.Entrepot;
import tn.project.easy.stock.easy.stock.data.Produit;


public interface ArrivageServices {
List<Arrivage> getArrivages();
List<Produit> getArrivageProduits(Long arrId);
List<Entrepot> getArrivageEntrepots(Long arrId);
}