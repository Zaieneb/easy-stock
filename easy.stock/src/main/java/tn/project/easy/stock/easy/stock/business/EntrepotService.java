package tn.project.easy.stock.easy.stock.business;

import java.util.List;

import tn.project.easy.stock.easy.stock.business.exceptions.IncoherentCountryCityException;
import tn.project.easy.stock.easy.stock.business.exceptions.NotEnoughSpaceException;
import tn.project.easy.stock.easy.stock.business.exceptions.SameAddressException;
import tn.project.easy.stock.easy.stock.business.exceptions.UncoveredCityException;
import tn.project.easy.stock.easy.stock.business.exceptions.UncoveredCountryException;
import tn.project.easy.stock.easy.stock.data.Entrepot;
import tn.project.easy.stock.easy.stock.data.Produit;



public interface EntrepotService {
	void ajouterEntrepot(Entrepot e) throws SameAddressException,IncoherentCountryCityException, UncoveredCityException,UncoveredCountryException;
    Long nbreEntrepotParVille(String ville);
    Long nbreEntrepotParPays(String pays); 
    void affecterProduitToEntrepot(Produit p,Entrepot e) throws  NotEnoughSpaceException;
	Double getRemainingSpace(Entrepot e);
	List<String> getEntrepotsWithCapacityAlert();
	Double getOccupiedSpace(Entrepot e);
	void deplacerProduits(Long entrepotId1, Long entrepotId2) ;
} 