package tn.project.easy.stock.easy.stock.business;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.project.easy.stock.easy.stock.business.exceptions.NotEnoughSpaceException;
import tn.project.easy.stock.easy.stock.business.exceptions.SameAddressException;
import tn.project.easy.stock.easy.stock.data.Address;
import tn.project.easy.stock.easy.stock.data.AddressRepository;
import tn.project.easy.stock.easy.stock.data.Entrepot;
import tn.project.easy.stock.easy.stock.data.EntrepotRepository;
import tn.project.easy.stock.easy.stock.data.EspaceEntrepot;
import tn.project.easy.stock.easy.stock.data.Produit;


@Service
public class EntrepotServiceImpl implements EntrepotService {
	@Autowired
	private EntrepotRepository er;
	@Autowired
	private AddressRepository ar;
	private EspaceEntrepot es;
	@Autowired
	private ProduitServices produitServices;


	@Override
	public void ajouterEntrepot(Entrepot e) throws SameAddressException {
		Address a = e.getAdresse();
	if(ar.countByNumberAndRueIgnoreCaseAndVilleIgnoreCaseAndPaysIgnoreCaseAndCpIgnoreCase(a.getNumber(),a.getRue(), a.getVille(), a.getPays(),a.getCp()) > 0) {
		throw new SameAddressException("cette adresse existe déjà");
	}
	else er.save(e)	;
	}
	
	@Override
	public Double getOccupiedSpace(Entrepot e) {

		List<Produit> produits = e.getProduits();
		Double capacity =  produits.stream().map(p -> produitServices.getDimensionProduit(p)).reduce((double) 0, (a, b) -> a+b);
		return capacity;
		
	}

	@Override
	public Double getRemainingSpace(Entrepot e) {
		 
		return es.getGlobalCapacity(e) - getOccupiedSpace(e);
	}

	@Override
	public List<String> getEntrepotsWithCapacityAlert() {
		
		List<String> entrepotsWAlert = er.findAll().stream().filter(ent -> getRemainingSpace(ent) == 0.2 * es.getGlobalCapacity(ent)).map( ent -> ent.getNom()).collect(Collectors.toList());
		return entrepotsWAlert;
	}


	@Override
	public Long nbreEntrepotParVille(String ville) {
		return er.nbreEntrepotParVille(ville);
	}

	@Override
	public Long nbreEntrepotParPays(String pays) {
		
		return er.nbreEntrepotParPays(pays);
	}

	@Override
	public void affecterProduitToEntrepot(Produit p,Entrepot e) throws  NotEnoughSpaceException{
		if(getRemainingSpace(e) > produitServices.getDimensionProduit(p)) {
		e.getProduits().add(p);
		er.save(e);}
		else throw new NotEnoughSpaceException("l'espace de l'entrepot n'est pas suffisant");
	}

	@Override
	public void deplacerProduits(Long entrepotId1, Long entrepotId2) {
		Entrepot e1 = er.getOne(entrepotId1);
		Entrepot e2 = er.getOne(entrepotId2);
		System.out.println(e1);
		System.out.println(e2);
		
		e1.getProduits().stream().forEach(p -> {
		if(getRemainingSpace(e2) > produitServices.getDimensionProduit(p)) {
			e2.getProduits().add(p);
			e1.getProduits().remove(p);
		}
		else System.out.print("entrepot complet");
		});
		er.save(e1);
		er.save(e2);
		
		/*e1.getProduits().stream().map( p -> e2.getProduits().add(p));
		//e1.setProduits(null);
		er.save(e1);
		er.save(e2);*/
		
	}


}

