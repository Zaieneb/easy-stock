package tn.project.easy.stock.easy.stock.business;
import java.util.List;

import tn.project.easy.stock.easy.stock.business.exceptions.ProduitExistDansEntrepotException;
import tn.project.easy.stock.easy.stock.data.Produit;

public interface ProduitServices {
void ajouterProduit(Produit p);
void supprimerProduit(Long produitId) throws ProduitExistDansEntrepotException;
List<Produit> getProduits();
List<Integer> nbreProduitParEntrepot();
Long moyProduitParEntrepot();
Double pourcentagePdtAffectés();
Integer avgQuantite();
Double getDimensionProduit(Produit p);
List<Produit> getUnaffectedProduits();

}