package tn.project.easy.stock.easy.stock.business.exceptions;

public class NotEnoughSpaceException extends Exception{
	public NotEnoughSpaceException(String message){
		super(message);
	}

}
