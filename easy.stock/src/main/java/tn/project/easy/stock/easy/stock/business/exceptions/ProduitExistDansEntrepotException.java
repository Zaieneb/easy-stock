package tn.project.easy.stock.easy.stock.business.exceptions;

public class ProduitExistDansEntrepotException extends Exception {
	public ProduitExistDansEntrepotException(String message) {
		super(message);
	}

}
