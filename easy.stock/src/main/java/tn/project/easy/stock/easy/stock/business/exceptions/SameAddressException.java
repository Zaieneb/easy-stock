package tn.project.easy.stock.easy.stock.business.exceptions;

public class SameAddressException extends Exception {
	public SameAddressException(String e) {
		super(e);
	}

}
