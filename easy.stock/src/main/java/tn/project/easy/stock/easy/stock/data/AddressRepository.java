package tn.project.easy.stock.easy.stock.data;

public interface AddressRepository {
	Long countByNumberAndRueIgnoreCaseAndVilleIgnoreCaseAndPaysIgnoreCaseAndCpIgnoreCase(Integer number,String rue,String ville,String pays,String cp);

}
