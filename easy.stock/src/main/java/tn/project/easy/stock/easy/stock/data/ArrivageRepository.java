package tn.project.easy.stock.easy.stock.data;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArrivageRepository extends JpaRepository<Arrivage, Long> {

}