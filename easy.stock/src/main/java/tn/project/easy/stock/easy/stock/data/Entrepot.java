package tn.project.easy.stock.easy.stock.data;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.ManyToAny;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Entrepot {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
private Long id;
private String nom;
@JsonIgnore
@OneToOne(mappedBy = "entrepot", cascade = CascadeType.ALL)
private Address adresse;
@JsonIgnore
@ManyToMany(mappedBy = "entrepots")
private List<Produit> produits;

public List<Produit> getProduits() {
	return produits;
}
public void setProduits(List<Produit> produits) {
	this.produits = produits;
}
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
public Address getAdresse() {
	return adresse;
}
public void setAdresse(Address adresse) {
	this.adresse = adresse;
}

}


