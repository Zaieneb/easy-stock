package tn.project.easy.stock.easy.stock.data;
import javax.persistence.Entity;

@Entity
public class EntrepotRectangulaire extends Entrepot {

	private double length;
	private double height;
	private double width;
	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		this.length = length;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width = width;
	}
	
}

