package tn.project.easy.stock.easy.stock.data;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface EntrepotRepository extends JpaRepository<Entrepot,Long>{
	@Query("select count (e) from Entrepot e where TRIM(UPPER(e.adresse.number))=TRIM(UPPER(?1)) "
			+ "and TRIM(UPPER(e.adresse.rue))=TRIM(UPPER(?2)) and "
			+ "TRIM(UPPER(e.adresse.ville))=TRIM(UPPER(?3)) and "
			+ "TRIM(UPPER(e.adresse.pays))=TRIM(UPPER(?4)) and "
			+ "TRIM(UPPER(e.adresse.cp))=TRIM(UPPER(?5))") //JPQL
	Long checkDuplicateAddress(Integer number, String rue, String ville, String pays, String cp);
	@Query(nativeQuery = true,value="select count(*) from entrepot e join address a on e.id = a.entrepot_id where a.ville=?1 group by a.ville ")
	Long nbreEntrepotParVille(String ville); 
	@Query(nativeQuery = true,value="select count(*) from entrepot e join address a on e.id = a.entrepot_id where a.pays=?1 group by a.pays ")
	Long nbreEntrepotParPays(String pays); 
	

}