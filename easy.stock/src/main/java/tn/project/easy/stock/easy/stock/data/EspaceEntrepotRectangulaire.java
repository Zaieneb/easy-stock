package tn.project.easy.stock.easy.stock.data;

public class EspaceEntrepotRectangulaire implements EspaceEntrepot {

	@Override
	public Double getGlobalCapacity(Entrepot e) {
		Double volume = 0.0;
		if (e instanceof EntrepotRectangulaire) {
		
		volume = ((EntrepotRectangulaire) e).getHeight() * ((EntrepotRectangulaire) e).getLength() * ((EntrepotRectangulaire) e).getWidth();
		}
		return volume;

	}

}
