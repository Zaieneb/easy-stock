package tn.project.easy.stock.easy.stock.data;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Produit {
     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
    private Long reference;
    private String label;
    private Double prix;
    private String description;
    private Integer quantite;
    private Double width;
    private Double length;
    private Double height;
    public Double getWidth() {
		return width;
	}
	public void setWidth(Double width) {
		this.width = width;
	}
	public Double getLength() {
		return length;
	}
	public void setLength(Double length) {
		this.length = length;
	}
	public Double getHeight() {
		return height;
	}
	public void setHeight(Double height) {
		this.height = height;
	}
	@Enumerated
    private TypeEmballage emballage;
    @JsonIgnore
    @ManyToMany
    @JoinTable( name= "entrepot_produit", joinColumns = {
    		@JoinColumn (name="produit_id",referencedColumnName = "id")},
    		inverseJoinColumns = {
    		@JoinColumn (name="entrepot_id",referencedColumnName = "id")	}	)
    private List<Entrepot> entrepots;
    @JsonIgnore
    @JoinColumn(name="arrivage_id",referencedColumnName = "id")
    @ManyToOne(optional= false)
    private Arrivage arrivageId;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getReference() {
		return reference;
	}
	public void setReference(Long reference) {
		this.reference = reference;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public Double getPrix() {
		return prix;
	}
	public void setPrix(Double prix) {
		this.prix = prix;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getQuantite() {
		return quantite;
	}
	public void setQuantite(Integer quantite) {
		this.quantite = quantite;
	}
	public TypeEmballage getEmballage() {
		return emballage;
	}
	public void setEmballage(TypeEmballage emballage) {
		this.emballage = emballage;
	}
	public List<Entrepot> getEntrepots() {
		return entrepots;
	}
	public void setEntrepots(List<Entrepot> entrepots) {
		this.entrepots = entrepots;
	}
	public Arrivage getArrivageId() {
		return arrivageId;
	}
	public void setArrivageId(Arrivage arrivageId) {
		this.arrivageId = arrivageId;
	}
    
}

